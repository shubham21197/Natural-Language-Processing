# Natural-Language-Processing
Python scripts for text vectorization and application of NLP to them for functions like Spam Filtering, Document Classification etc.
The dataset is not mine, so I cannot provide it, but here is the format:

**Natural Language Processing and Vectorization:**

ham\tNormal Text Message

ham\tNormal Text Message

spam\tSpam Message
